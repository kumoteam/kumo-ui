# Kumo UI #

Technology stack:
 - Angular - 4.3
 - TypeScript - 2.4
 - RxJS - 5.4
 - Webpack 3.1
 - SCSS

## Development Run ##

To run application locally for development do the following:

1) Clone the repository
```sh
$ git clone git@bitbucket.org:sharad_brat/angular-boilerplate.git
$ cd angular-boilerplate
```
2) Install dependencies (Note that Node.js LTS version should be installed on your machine)
```sh
$ npm install
```
3) Run json-server with mock data
```sh
$ npm run devapi
```
4) Run application using webpack-dev-server
```sh
$ npm run dev
```
5) See your application running on [localhost:8080](http://localhost:8080)

## Production Build & Serve ##

Build production bundle(your bundle will be located in dist folder):
```sh
$ npm run prod
```
Serve built bundle:
```sh
$ npm run serve
```

## Testing ##

Run unit tests:
```sh
$ npm run test
```

Run e2e tests:
```sh
$ npm run webdriver-update
$ npm run e2e
```
