const helpers = require('./helpers');
const webpackMerge = require('webpack-merge');
const commonConfig = require('./webpack.common.js');

const LoaderOptionsPlugin = require('webpack/lib/LoaderOptionsPlugin');
const DefinePlugin = require('webpack/lib/DefinePlugin');
const DashboardPlugin = require('webpack-dashboard/plugin');

const ENV = process.env.ENV = process.env.NODE_ENV = 'development';

module.exports = webpackMerge(commonConfig, {
  devtool: 'cheap-module-source-map',

  output: {
    path: helpers.root('dist'),
    filename: '[name].bundle.js',
    sourceMapFilename: '[file].map',
    chunkFilename: '[id].chunk.js'
  },

  module: {
    rules: [
      /**
       * Sass loader support for *.scss files (styles directory only)
       * Loads external sass styles into the DOM, supports HMR
       */
      {
        test: /\.component\.scss$/,

        use: [
          {
            loader: 'postcss-loader',
            options: {config: helpers.root('config', 'postcss.config.js')}
          },
          'sass-loader']
      },
      {
        test: /\.scss$/,

        use: ['style-loader', 'css-loader',
          {
            loader: 'postcss-loader',
            options: {config: helpers.root('config', 'postcss.config.js')}
          },
          'sass-loader'],
        include: [helpers.root('src', 'styles')],
        exclude: [helpers.root('src', 'app')]
      }
    ]
  },

  plugins: [

    /**
     * Plugin: LoaderOptionsPlugin (experimental)
     *
     * See: https://gist.github.com/sokra/27b24881210b56bbaff7
     */
    new LoaderOptionsPlugin({
      debug: true,
      options: {}
    }),

    /**
     * Plugin: DefinePlugin
     * Description: Define free variables.
     * Useful for having development builds with debug logging or adding global constants.
     *
     * Environment helpers
     *
     * See: https://webpack.github.io/docs/list-of-plugins.html#defineplugin
     */
    new DefinePlugin({
      'process.env': {
        'ENV': JSON.stringify(ENV)
      }
    }),

    /**
     * Plugin: WebpackDashboard
     * Description: Complete webpack information
     *
     * See: https://github.com/FormidableLabs/webpack-dashboard
     */
    new DashboardPlugin()

  ],

  devServer: {
    historyApiFallback: true,
    watchOptions: {
      // if you're using Docker you may need this
      // aggregateTimeout: 300,
      // poll: 1000,
      ignored: /node_modules/
    }
  }

});
