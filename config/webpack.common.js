const helpers = require('./helpers');

const ContextReplacementPlugin = require('webpack/lib/ContextReplacementPlugin');
const CommonsChunkPlugin = require('webpack/lib/optimize/CommonsChunkPlugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const LoaderOptionsPlugin = require('webpack/lib/LoaderOptionsPlugin');

module.exports = {

  entry: {
    polyfills: './src/polyfills.ts',
    vendor: './src/vendor.ts',
    app: './src/main.ts'
  },

  resolve: {

    /**
     * An array of extensions that should be used to resolve modules.
     */
    extensions: ['.ts', '.js', '.css', '.scss', '.html'],

    /**
     * An array of directory names to be resolved to the current directory
     */
    modules: [helpers.root('src'), helpers.root('node_modules')]
  },

  module: {
    rules: [

      /**
       * Typescript loader support for .ts
       * Component Template/Style integration using `angular2-template-loader`
       */
      {
        test: /\.ts$/,
        use: ['awesome-typescript-loader', 'angular2-template-loader'],
        exclude: [/\.(spec|e2e)\.ts$/]
      },

      /**
       * To string and sass loader support for *.scss files (from Angular components)
       * Returns compiled css content as string
       */
      {
        test: /\.scss$/,
        use: ['to-string-loader', 'css-loader', 'sass-loader'],
        exclude: [helpers.root('src', 'styles')]
      },

      /**
       * Raw loader support for *.html
       * Returns file content as string
       */
      {
        test: /\.html$/,
        use: 'raw-loader',
        exclude: [helpers.root('src/index.html')]
      },

      /**
       * File loader for supporting images, for example, in CSS files.
       */
      {
        test: /\.(jpg|png|gif)$/,
        use: 'file-loader'
      },

      /**
       * File loader for supporting fonts, for example, in CSS files.
       */
      {
        test: /\.(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
        loader: 'file-loader?name=fonts/[name].[ext]'
      }
    ],
  },

  plugins: [

    /**
     * To separate app, vendor and polyfills code from each other in different files
     * */
    new CommonsChunkPlugin({
      name: ['app', 'vendor', 'polyfills']
    }),

    /**
     * Provides context to Angular's use of System.import
     *
     * See: https://github.com/angular/angular/issues/11580
     */
    new ContextReplacementPlugin(
      /**
       * The (\\|\/) piece accounts for path separators in *nix and Windows
       */
      /angular(\\|\/)core(\\|\/)@angular/,
      helpers.root('src'), // location of your src
      {
        /**
         * Your Angular Async Route paths relative to this root directory
         */
      }
    ),

    /**
     * Copies project static assets.
     */
    new CopyWebpackPlugin([{from: 'resource', to: 'resource'}]),

    /**
     * Simplifies creation of HTML files to serve your webpack bundles.
     * This is especially useful for webpack bundles that include a hash in the filename
     * which changes every compilation.
     */
    new HtmlWebpackPlugin({
      template: 'src/index.html'
    }),

    new LoaderOptionsPlugin({
      options: {
        htmlLoader: {
          minimize: false // workaround for ng2
        }
      }
    })
  ]

};
