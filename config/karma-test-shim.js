// This is the entry file for webpack test. When the test runs, it will
// compile and bundle all the tests here. So we need to do some setup

Error.stackTraceLimit = Infinity;

require('core-js/es6');
require('core-js/es7/reflect');

require('zone.js/dist/zone');
require('zone.js/dist/long-stack-trace-zone');
require('zone.js/dist/proxy');
require('zone.js/dist/sync-test');
require('zone.js/dist/jasmine-patch');
require('zone.js/dist/async-test');
require('zone.js/dist/fake-async-test');

require('rxjs/Rx');

const testing = require('@angular/core/testing');
const browser = require('@angular/platform-browser-dynamic/testing');

// Init test env for Angular
testing.TestBed.initTestEnvironment(
  browser.BrowserDynamicTestingModule,
  browser.platformBrowserDynamicTesting()
);

// Setting require to recursively run in src directory, loading all the *.spec.ts files
const testContext = require.context('../src', true, /\.spec\.ts/);
testContext.keys().map(testContext);
