module.exports = function (config) {
  const webpackConf = require('./webpack.test.js')({env: 'test'});

  const configuration = {

    /**
     * Base path that will be used to resolve all patterns (e.g. files, exclude).
     */
    basePath: '',

    /**
     * List of used plugins.
     */
    plugins: [
      require('karma-jasmine'),
      require('karma-webpack'),
      require('karma-coverage'),
      require('karma-remap-coverage'),
      require('karma-mocha-reporter'),
      require('karma-sourcemap-loader'),
      require('karma-phantomjs-launcher'),
      require('karma-jasmine-html-reporter')
    ],

    /**
     * Frameworks to use
     *
     * available frameworks: https://npmjs.org/browse/keyword/karma-adapter
     */
    frameworks: ['jasmine'],

    /**
     * List of files / patterns to load in the browser
     *
     * we are building the test environment in ./karma-test-shim.js
     */
    files: [
      {pattern: './config/karma-test-shim.js', watched: false}
    ],

    /**
     * Preprocess matching files before serving them to the browser
     * available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
     */
    preprocessors: {'./config/karma-test-shim.js': ['coverage', 'webpack', 'sourcemap']},

    /**
     * Webpack Config at ./webpack.test.js
     */
    webpack: webpackConf,

    coverageReporter: {
      type: 'in-memory'
    },

    remapCoverageReporter: {
      'text-summary': null,
      json: './coverage/coverage.json',
      html: './coverage/html'
    },

    /**
     * Webpack please don't spam the console when running in karma!
     */
    webpackMiddleware: {
      /**
       * webpack-dev-middleware configuration
       * i.e.
       */
      noInfo: true,
      /**
       * and use stats to turn off verbose output
       */
      stats: {
        /**
         * options i.e.
         */
        chunks: false
      }
    },

    /**
     * Test results reporter to use
     *
     * possible values: 'dots', 'progress'
     * available reporters: https://npmjs.org/browse/keyword/karma-reporter
     */
    reporters: ['mocha', 'coverage', 'remap-coverage'],

    /**
     * Web server port.
     */
    port: 9876,

    /**
     * enable / disable colors in the output (reporters and logs)
     */
    colors: true,

    /**
     * Level of logging
     * possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
     */
    logLevel: config.LOG_INFO,

    /**
     * enable / disable watching file and executing tests whenever any file changes
     */
    autoWatch: false,

    /**
     * start these browsers
     * available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
     */
    browsers: [
      'PhantomJS'
    ],

    /**
     * Continuous Integration mode
     * if true, Karma captures browsers, runs the tests and exits
     */
    singleRun: true
  };

  config.set(configuration);
};
